const mix = require('laravel-mix')

mix
    .webpackConfig({
        devtool: 'source-map'
    })
    .disableSuccessNotifications()
    .js('resources/js/main.js', `dist/js`)
    .sass('resources/scss/style.scss', `dist/css`)
    .sourceMaps(false)
    .browserSync({
        server: {
            baseDir: "dist"
        },
        files: "dist/**/*.*"
    })
